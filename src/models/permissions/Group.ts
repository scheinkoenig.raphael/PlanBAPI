import {Permission} from "./permission";
import {Member} from "./member";

export interface Group{
    name: string
    permissions: [Permission]
    members: [Member]
    id: string
}