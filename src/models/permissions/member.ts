import {Group} from "./Group";
import {Permission} from "./permission";

export interface Member{
    id: string
    auth0_id: string
    groups: [Group]
    permissions: [Permission]
}