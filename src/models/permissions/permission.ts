export interface Permission{
    id: string
    value: string
    description: string
}