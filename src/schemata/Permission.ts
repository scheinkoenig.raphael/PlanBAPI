import * as mongosse from "mongoose"
import {Schema} from "./Schema";
import * as mongoose from "mongoose";

export class Permission implements Schema{
    getName(): string {
        return "Permission";
    }

    getSchema(): mongoose.Schema {
        return new mongosse.Schema({
            value: String,
            description: String
        });
    }

    getModel(): mongoose.Model{
        return mongoose.model(this.getName(),this.getSchema());
    }

}


