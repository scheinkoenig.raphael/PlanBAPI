import * as mongoose from "mongoose";
export interface Schema{
   getName(): string;
   getSchema(): mongoose.Schema;
   getModel(): mongoose.Model;
}