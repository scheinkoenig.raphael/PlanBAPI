import * as mongoose from "mongoose";
import {Schema} from "./Schema";

export class Group implements Schema{

     getSchema(): mongoose.Schema{
        return new mongoose.Schema({
            name: String,
            permissions: Array,
            members: Array
        });
    }
     getName(): string{
        return "Group";
    }
    getModel(): mongoose.Model{
        return mongoose.model(this.getName(),this.getSchema());
    }
}