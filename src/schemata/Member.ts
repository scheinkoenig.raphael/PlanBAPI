import * as mongoose from "mongoose"
import {Schema} from "./Schema";
import * as jwt from "jsonwebtoken";
import {API} from "../index";

export class Member implements Schema{
    getName(): string {
        return "Member";
    }

    getSchema(): mongoose.Schema {
        return new mongoose.Schema({
            userid: String,
            groups: Array,
            permissions: Array
        });
    }
    getModel(): mongoose.Model{
       return mongoose.model(this.getName(),this.getSchema());
    }



    }