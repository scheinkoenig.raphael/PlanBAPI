import * as express from "express";
import {Permission} from "./schemata/Permission";
import * as mongoose from "mongoose";
import {Member} from "./schemata/Member";
import {Group} from "./schemata/Group";
import {Config, d_config} from "./utils/config_util";
import * as fs from "fs";


export class API {
    get public_key(): Buffer {
        return this._public_key;
    }
    get private_key(): Buffer {
        return this._private_key;
    }
    get models(): Map<string, mongoose.Model> {
        return this._models;
    }

    private _private_key: Buffer = fs.readFileSync('private.key');
    private _public_key: Buffer = fs.readFileSync('public.pub');
    private app:express.Application = express();
    private config: Config = d_config;
    private _models: Map <string, mongoose.Model> = new Map<string, mongoose.Model>();

    private connectionOptions: mongoose.ConnectionOptions = {
        config: {
            autoIndex: false
        }
    };

    constructor(){
        //mongoose.connect(`mongodb://${this.config.mongo_user}:${this.config.mongo_password}@${this.config.mongo_url}/${this.config.mongo_database}`, this.connectionOptions);
        this.setupModels();


    }

    setupModels(){
        this._models.set(new Permission().getName(),new Permission().getModel());
        this._models.set(new Member().getName(), new Member().getModel());
        this._models.set(new Group().getName(), new Group().getModel());
    }

    registerRoutes(){

    }



}