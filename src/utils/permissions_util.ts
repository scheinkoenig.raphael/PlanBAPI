import {API} from "../index";
import * as express from "express";
import * as mongoose from "mongoose";
import User from "../schemata/Member";
import Member from "../models/permissions/member";
import Group from "../schemata/Group";
import GGroup from "../models/permissions/Group";

export class Permissions_util{
    async hasUserPermission(api: API): boolean{
        return true
    }

    static getPermission(userid:string): Promise<[string]>{
        return new Promise<[string]>(((resolve, reject) => {
            User.getModel().findOne({userid:userid},(err:string, user: Member) =>{
                if (err) throw new Error(err);
                let permissions: [string] = [];
                permissions.push.apply(permissions, user.permissions);
                user.groups.forEach(el => {
                    Group.getModel().findOne({userid:el}, (group:GGroup) => {
                        permissions.push.apply(permissions,group);
                    })
                }).then(() =>{
                    resolve(permissions);
                });

            })
        }))
    }

    static permissions_check(api: API){
        return (req: express.Request, res: express.Response, next: express.NextFunction){

        }
    }

}