
export interface Config{
    mongo_user: string
    mongo_password: string
    mongo_url: string
    mongo_database
}

export const d_config: Config = {
    mongo_password: "",
    mongo_url: "",
    mongo_user: "",
    mongo_database: ""
}