import {API} from "../index";
import * as jwt from "jsonwebtoken"
import * as express from "express";
import * as admin from "firebase-admin";
import * as firebase from "firebase";

export class Auth_util{
    isJWTvalid(api: API, token: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            jwt.verify(token, api.public_key, ((err, decoded) => {
                if (err) resolve(false);
                resolve(true);
            }));
        })
    }

     signToken(api: API, payload: any):string{
        return jwt.sign(payload, api.private_key, {algorithm: 'RS256'});
    }


    authuser(token: string){
        let serviceAccount = require("path/to/serviceAccountKey.json");

        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://behaim-test-iii.firebaseio.com"
        });

        admin.auth().verifyIdToken(token,true)

            .then(decodedToken => {
                decodedToken.permission =
            })
    }

}