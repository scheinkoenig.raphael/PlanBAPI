import {expect} from 'chai';
import {API} from "../src";
import {Auth_util} from "../src/utils/auth_util";
import * as jwt from "jsonwebtoken";
import 'mocha';
import {it} from "mocha";

const api: API =  new API();
const auth: Auth_util = new Auth_util();
let token:string = "";
describe('Auth Test', () => {
    it('should return hello world', () => {
        token = auth.signToken(api,{string: "HIIII"});
        expect(token).to.not.null;
    });
    it('should verify token', () => {
        auth.isJWTvalid(api,token).then(resolve =>{
            expect(resolve).to.equal(true);
        });
    })
    it('should generate a second token', () => {
        token = jwt.sign({msg: "Hiiii"}, "ASDGF");
    })
    it('should not verify token', () => {
        auth.isJWTvalid(api,token).then(resolve =>{
            expect(resolve).to.equal(false);
        });
    })

});